/************ Trigger para calcular la SUMA del total ***********/

CREATE TRIGGER movimiento.SumarMonto
ON movimiento.DetallePedido
AFTER INSERT
AS
DECLARE @subtotal DECIMAL(10,2)
SET @subtotal = ((SELECT precioUnitario
from inserted) * (SELECT cantidad
FROM inserted))
UPDATE movimiento.Pedido 
SET total += @subtotal 
WHERE codigoPedido IN (SELECT codigoPedido
from inserted) 
GO

/************ Trigger para calcular la Resta del total ***********/

CREATE TRIGGER movimiento.RestarMonto
ON movimiento.DetallePedido
AFTER DELETE
AS
DECLARE @subtotal DECIMAL(10,2)
SET @subtotal = ((SELECT precioUnitario
from deleted) * (SELECT cantidad
FROM deleted))
UPDATE movimiento.Pedido 
SET total -= @subtotal 
WHERE codigoPedido IN (SELECT codigoPedido
from deleted)
GO

/************ Trigger para crear DEUDOR y aumentar su campo saldoDeudor ***********/

CREATE TRIGGER [movimiento].[InsertarDeudor]
ON [movimiento].[Pedido]
AFTER UPDATE
AS
  IF (select TipoPedido
from inserted) = 'credito' 
  BEGIN
    IF EXISTS (select codigoCliente
    from movimiento.Deudor
    where codigoCliente IN (select codigoCliente
    from inserted))
	  BEGIN
        DECLARE @diferencia DECIMAL(9,2)
        DECLARE @saldoDeudorUpdate DECIMAL(9,2)
        DECLARE @limite DECIMAL(9,2)
        SET @saldoDeudorUpdate = (select saldoDeudor
        from movimiento.Deudor
        where codigoCliente IN(select codigoCliente
        from inserted))
        SET @diferencia = ((select total
        from inserted) - @saldoDeudorUpdate)
        SET @limite = @diferencia + @saldoDeudorUpdate
        if @limite > (select limiteCredito
        from movimiento.Deudor
        where codigoCliente in (select codigoCliente
        from inserted))
		  begin
            rollback
            print 'LIMITE DE CREDITO EXCEDIDO'
        end
		  else
		  begin
            UPDATE movimiento.Deudor SET saldoDeudor += @diferencia 
			  where codigoCliente IN (select codigoCliente
            from inserted)
        end
    END
	  ELSE
	  BEGIN
        DECLARE @garante INTEGER;
        DECLARE @cliente INTEGER;
        DECLARE @limiteCredito DECIMAL(9,2);
        DECLARE @saldoDeudor DECIMAL(9,2);
        SET @garante = (select codigoGarante
        from catalogo.Cliente
        WHERE numeroCliente IN (SELECT numeroCliente
        FROM inserted));
        SET @cliente = (select numeroCliente
        from inserted);
        SET @limiteCredito = (select limiteCredito
        from catalogo.Cliente
        WHERE numeroCliente IN (SELECT numeroCliente
        FROM inserted));
        SET @saldoDeudor = (select total
        from inserted);
        if @saldoDeudor > @limiteCredito
		  BEGIN
            print 'LIMITE DE CREDITO EXCEDIDO'
            rollback
        END
		  ELSE
		  BEGIN
            IF EXISTS (select codigoCliente
            from movimiento.Deudor
            where codigoCliente = @garante)
				print('EL GARANTE NO PUEDE SER DEUDOR')
				
			 ELSE
			 BEGIN
                INSERT INTO movimiento.Deudor
                VALUES(@cliente, @garante, @limiteCredito, @saldoDeudor);
            END
        END
    END
END
GO

/************ Trigger para reducir el campo saldoDeudor al realizar un pago ***********/

CREATE TRIGGER movimiento.ReduceSaldoDeudor
ON movimiento.Pago
AFTER INSERT
AS
UPDATE movimiento.Deudor SET saldoDeudor -= (select valorPago
from inserted)
IF (select saldoDeudor
from movimiento.Deudor) <= 0
BEGIN
    DELETE FROM movimiento.Deudor where codigoCliente in (select codigoCliente
    from inserted)
END
GO
