		
-- Se pone por default el constraint de 'contado' a la columna TipoPedido
ALTER TABLE movimiento.Pedido ADD TipoPedido varchar(10) 
CONSTRAINT default_value_tipo_pedido DEFAULT 'contado' WITH VALUES;

-- Agregar un constraint para valores de tipo pedido
ALTER TABLE movimiento.Pedido 
ADD CONSTRAINT check_tipo_pedido check(TipoPedido = 'contado' OR TipoPedido= 'credito')
SELECT * FROM movimiento.Pedido

select * from movimiento.pedido
/* si se elimina un pedido, deben eliminarse todos los detalles de pedido?
--SOLUCION : CASCADE en la tabla pedido

--  cuando se inserta en PAGO no se va a eliminar de DEUDOR porque se tiene que eliminar
el padre para que se eliminen los hijos...pero pago es hijo de deudor*/
















-- creaci�n del trigger para calcular la SUMA del total
CREATE TRIGGER movimiento.SumarMonto
ON movimiento.DetallePedido
AFTER INSERT
AS
 DECLARE @subtotal DECIMAL(10,2)
 SET @subtotal = ((SELECT precioUnitario from inserted) * (SELECT cantidad FROM inserted))
 --debe restarse el descuento que tiene cada cliente
 UPDATE movimiento.Pedido 
 SET total += @subtotal 
 WHERE codigoPedido IN (SELECT codigoPedido from inserted)

-- creaci�n del trigger para calcluar la RESTA del total
CREATE TRIGGER movimiento.RestarMonto
ON movimiento.DetallePedido
AFTER DELETE
AS
 DECLARE @subtotal DECIMAL(10,2)
 SET @subtotal = ((SELECT precioUnitario from deleted) * (SELECT cantidad FROM deleted))
 UPDATE movimiento.Pedido 
 SET total -= @subtotal 
 WHERE codigoPedido IN (SELECT codigoPedido from deleted)

 SELECT * FROM movimiento.DetallePedido
 SELECT * FROM movimiento.Pedido
 
 insert into movimiento.DetallePedido values(2, 9, 2, 3, 4)
 delete from movimiento.DetallePedido where linea=3
 insert into movimiento.Pedido values(4, GETDATE(), 4, 0, 'contado')

 select * from catalogo.Cliente

 CREATE TABLE movimiento.Deudor (
	codigoCliente Integer NOT NULL,
	codigoGarante Integer NOT NULL,
	[limiteCredito] Decimal(9,2),
	saldoDeudor Decimal(9,2)
 )on [PRIMARY];

ALTER TABLE movimiento.Deudor 
ADD Constraint [pk_PuedeSer] Primary Key (codigoCliente)

Alter table catalogo.Cliente 
add Constraint [debetener] foreign key([codigoGarante]) 
references catalogo.[Cliente] ([numeroCliente])  
on update no action on delete no action 

ALTER TABLE movimiento.Deudor
ADD CONSTRAINT [puedeTener] Foreign Key (codigoGarante)
REFERENCES catalogo.Cliente(numeroCliente)
ON DELETE cascade  ON UPDATE NO ACTION


CREATE TABLE movimiento.Pago
(
	codigoCliente integer,
	fechaPago DATE,
	valorPago Decimal(9,2)
	PRIMARY KEY(codigoCliente, fechaPago)
)on [PRIMARY];

ALTER TABLE movimiento.Pago
ADD CONSTRAINT [fk_idCliente] FOREIGN KEY (codigoCliente)
REFERENCES movimiento.Deudor(codigoCliente)
ON DELETE CASCADE ON UPDATE NO ACTION


-- TRIGGER PARA EL PUNTO 6 
CREATE TRIGGER [movimiento].[InsertarDeudor]
ON [movimiento].[Pedido]
AFTER UPDATE
AS

  IF (select TipoPedido from inserted) = 'credito' 
  BEGIN
	  IF EXISTS (select codigoCliente from movimiento.Deudor 
	  where codigoCliente IN (select codigoCliente from inserted))
	  BEGIN
		  DECLARE @diferencia DECIMAL(9,2)
		  DECLARE @saldoDeudorUpdate DECIMAL(9,2)
		  DECLARE @limite DECIMAL(9,2)

		  SET @saldoDeudorUpdate = (select saldoDeudor from movimiento.Deudor 
		  where codigoCliente IN(select codigoCliente from inserted))
		  SET @diferencia = ((select total from inserted) - @saldoDeudorUpdate)
		  SET @limite = @diferencia + @saldoDeudorUpdate
		  if @limite > (select limiteCredito from movimiento.Deudor 
		  where codigoCliente in (select codigoCliente from inserted))
		  begin 
			rollback
			--print 'L�MITE DE CREDITO EXCEDIDO---aqu� debe ir un rollback'
		  end
		  else
		  begin
			  UPDATE movimiento.Deudor SET saldoDeudor += @diferencia 
			  where codigoCliente IN (select codigoCliente from inserted)
		  end
	  END
	  ELSE
	  BEGIN
		  DECLARE @garante INTEGER; 
		  DECLARE @cliente INTEGER;
		  DECLARE @limiteCredito DECIMAL(9,2);
		  DECLARE @saldoDeudor DECIMAL(9,2);

		  SET @garante = (select codigoGarante from catalogo.Cliente 
		  WHERE numeroCliente IN (SELECT numeroCliente FROM inserted));
		  SET @cliente = (select numeroCliente from inserted);
		  SET @limiteCredito = (select limiteCredito from catalogo.Cliente
		  WHERE numeroCliente IN (SELECT numeroCliente FROM inserted));
		  SET @saldoDeudor = (select total from inserted);
		  if @saldoDeudor > @limiteCredito
		  BEGIN
			--print 'L�MITE DE CREDITO EXCEDIDO----aqu� debe ir un rollback'
			rollback
		  END
		  ELSE
		  BEGIN
			 IF EXISTS (select codigoCliente from movimiento.Deudor 
			 where codigoCliente = @garante)
				print('EL GARANTE NO PUEDE SER DEUDOR')
				
			 ELSE
			 BEGIN
			  INSERT INTO movimiento.Deudor VALUES(@cliente, @garante, @limiteCredito, @saldoDeudor);
			 END
		  END
	  END
	END

	SELECT * FROM catalogo.Cliente
	select * from movimiento.Pedido
	select * from movimiento.DetallePedido
		UPDATE catalogo.Cliente SET codigoGarante = 2 where numeroCliente=1
 INSERT INTO movimiento.Pedido values (1, GETDATE(), 4, 0, 'credito')
 INSERT INTO movimiento.DetallePedido values (6, 3, 2, 3, 4)
  INSERT INTO movimiento.DetallePedido values (4, 5, 2, 12, 4)
 SELECT * FROM movimiento.Deudor;

 -----------------------------------------------------------------------------
CREATE TRIGGER movimiento.ReduceSaldoDeudor
ON movimiento.Pago
AFTER INSERT
AS
 UPDATE movimiento.Deudor SET saldoDeudor -= (select valorPago from inserted)
 IF (select saldoDeudor from movimiento.Deudor) <= 0
 BEGIN
	DELETE FROM movimiento.Deudor where codigoCliente in (select codigoCliente from inserted)
 END

