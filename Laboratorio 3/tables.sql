/************ Creacion del campo TipoPed ***********/

ALTER TABLE movimiento.Pedido ADD TipoPedido varchar(10) 
CONSTRAINT default_value_tipo_pedido DEFAULT 'contado' WITH VALUES;

/************ Implementar integridad en la base Pedido ***********/

Alter table movimiento.[Pedido] add Constraint [Hace] foreign key([numeroCliente]) references catalogo.[Cliente] ([numeroCliente])  on update no action on delete no action 
go
Alter table movimiento.[RegistroProductoFabrica] add Constraint [posee] foreign key([numeroProducto]) references catalogo.[Producto] ([numeroProducto])  on update no action on delete no action 
go
Alter table movimiento.[DetallePedido] add Constraint [tiene] foreign key([numeroProducto]) references catalogo.[Producto] ([numeroProducto])  on update no action on delete no action
go
Alter table movimiento.[RegistroProductoFabrica] add Constraint [contiene] foreign key([numeroFabrica]) references catalogo.[Fabrica] ([numeroFabrica])  on update no action on delete no action 
go
Alter table movimiento.[DetallePedido] add Constraint [registra] foreign key([codigoPedido]) references movimiento.[Pedido] ([codigoPedido])  on update no action on delete cascade
go

/************ Limitar valores a "credito" y "contado" ***********/

ALTER TABLE movimiento.Pedido 
ADD CONSTRAINT check_tipo_pedido check(TipoPedido = 'contado' OR TipoPedido= 'credito')

/************ Creacion de la tabla DEUDOR ***********/

CREATE TABLE movimiento.Deudor (
    codigoCliente Integer NOT NULL,
    codigoGarante Integer NOT NULL,
    [limiteCredito] Decimal(9,2),
    saldoDeudor Decimal(9,2)
)on [PRIMARY];

--- Se añade la PFK a la tabla Deudor ---

ALTER TABLE movimiento.Deudor 
ADD Constraint [pk_PuedeSer] Primary Key (codigoCliente)

        --- esta parte hay que probar ---
ALTER TABLE movimiento.Deudor
ADD CONSTRAINT [esUn] Foreign Key (codigoCliente)
REFERENCES catalogo.Cliente(numeroCliente)
ON DELETE cascade ON UPDATE NO ACTION

ALTER TABLE movimiento.Deudor
ADD CONSTRAINT [puedeTener] Foreign Key (codigoGarante)
REFERENCES catalogo.Cliente(numeroCliente)

--- Creacion del campo garante en cliente ---

Alter table catalogo.Cliente 
add Constraint [debetener] foreign key([codigoGarante]) 
references catalogo.[Cliente] ([numeroCliente])  
on update no action on delete no action 

/************ Creacion de la tabla Pago ***********/

CREATE TABLE movimiento.Pago
(
    codigoPago integer,
	codigoCliente integer,
	fechaPago DATE,
	valorPago Decimal(9,2)
	PRIMARY KEY(codigoPago, codigoCliente, fechaPago)
)on [PRIMARY];

ALTER TABLE movimiento.Pago
ADD CONSTRAINT [fk_idCliente] FOREIGN KEY (codigoCliente)
REFERENCES movimiento.Deudor(codigoCliente)
ON DELETE CASCADE ON UPDATE NO ACTION
